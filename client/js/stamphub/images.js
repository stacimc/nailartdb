angular.module('nailArtApp')

	// Image controllers
	.controller('imageListController', function($scope, $state, $stateParams, Restangular, $filter, $modal) {
		// Use stateParams to determine if we are in the main Image List or viewing
		// the image list for a particular plate
		if ($stateParams.id) {
			// We are viewing the image list for a particular plate
			// GET /api/plates/id
			Restangular.one('plates', $stateParams.id).get().then(function(plate) {
				$scope.plate = plate;
				// GET /api/plates/id/images
				plate.getList('images').then(function(images) {
					$scope.images = images
					// Populate filteredImages with all the selected images at first
		        	$scope.filteredImages = images;
		        	$scope.figureOutImagesToDisplay();
				});
			});
		} else {
			// We are viewing the main Image list, so get all images
			// GET /api/images
			Restangular.all('images').getList().then(function(images) {
	        	$scope.images = images;

	        	// Populate filteredImages with all images at first
	        	$scope.filteredImages = images;
	        	$scope.figureOutImagesToDisplay();
	    	});
		}		

    	$scope.currentPage = 1;
    	$scope.imagesPerPage = 5;
    	$scope.displayedImages = [];

    	$scope.figureOutImagesToDisplay = function() {
    		var begin = (($scope.currentPage - 1) * $scope.imagesPerPage);
    		var end = begin + $scope.imagesPerPage;

    		$scope.displayedImages = $scope.filteredImages.slice(begin, end);
    	};

    	$scope.pageChanged = function() {
    		$scope.figureOutImagesToDisplay();
    	};

    	$scope.search = function() {
    		var results = $scope.images;
    		// Filter separately by each search term
    		var searchTerms = ($scope.searchtext).split(' ');
    		for (var i = 0; i < searchTerms.length; i++) {
    			results = $filter('filter')(results, {tags: searchTerms[i]});
    		}
    		$scope.filteredImages = results;
    		// Reset to page 1 of search results
    		$scope.currentPage = 1;
    		$scope.figureOutImagesToDisplay();
    	};

    	$scope.showModal = function(imageId) {
    		$scope.opts = {
    			templateUrl: '/static/stamphub/images/image_detail.html',
    			controller: 'imageDetailController',
    			resolve: {}
    		};

    		$scope.opts.resolve.id = function() {
    			return angular.copy(imageId)
    		}

    		var modalInstance = $modal.open($scope.opts);

    		modalInstance.result.then(function() {
    			console.log('pressed ok');
    		}, function() {
    			console.log('pressed cancel');
    		});
    	};
	})
	.controller('imageDetailController', function($scope, $state, $stateParams, Restangular, $modalInstance, id) {
		// GET /api/images/id
		Restangular.one('images', id).get().then(function(image) {
			$scope.image = image;
			$scope.tags = image.tags;
		});
		
		$scope.ok = function() {
			$modalInstance.close();
		};
	});