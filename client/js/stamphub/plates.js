angular.module('nailArtApp')
// Plate controllers
	.controller('plateListController', function($scope, $state, Restangular, $filter, $stateParams, CRUD_METHODS, plateModal) {
		// Use stateParams to determine if we are in the main plate list or viewing
		// the plate list for a particular collection
		if ($stateParams.id) {
			// We are in a subcontent panel, just get the plates for this collection
			Restangular.one('platecollections', $stateParams.id).get().then(function(collection) {
				$scope.collection = collection;
				// GET /api/platecollections/id
				collection.getList('plates').then(function(plates) {
					$scope.plates = plates;
					// Populate filteredPlates with all the selected plates at first
					$scope.filteredPlates = plates;
					$scope.figureOutPlatesToDisplay();

				});
			});
		} else {
			// We are in the main plate list, get all plates
			// GET /api/plates
			Restangular.all('plates').getList().then(function(plates) {
				$scope.plates = plates;

				// Populate filteredPlates with all plates at first
				$scope.filteredPlates = plates;
				$scope.figureOutPlatesToDisplay();
			});
		}
		

		$scope.currentPage = 1;
		$scope.platesPerPage = 4;
		$scope.displayedPlates = [];

		$scope.figureOutPlatesToDisplay = function() {
    		var begin = (($scope.currentPage - 1) * $scope.platesPerPage);
    		var end = begin + $scope.platesPerPage;

    		$scope.displayedPlates = $scope.filteredPlates.slice(begin, end);
    	};

    	$scope.pageChanged = function() {
    		$scope.figureOutPlatesToDisplay();
    	};

    	$scope.search = function() {
    		// Plates just has a basic search on plate number
    		$scope.filteredPlates = $filter('filter')($scope.plates, $scope.searchtext);
    		// Reset to page 1 of search results
    		$scope.currentPage = 1;
    		$scope.figureOutPlatesToDisplay();
    	};

    	// CRUD actions

    	$scope.addPlate = function() {
    		plateModal(CRUD_METHODS.add)
    		.then( function(newPlate){
    			// Push the new plate to the list so that it will be displayed without
    			// having to refresh the page first
                if(newPlate) {
                    $scope.filteredPlates.push(newPlate);
                    $scope.figureOutPlatesToDisplay();
                }
    		});
    	}
	})
	.controller('plateDetailController', function($scope, $state, $stateParams, $location, Restangular, plateModal, CRUD_METHODS) {
		// GET /api/plates/id
		Restangular.one('plates', $stateParams.id).get().then(function(plate) {
			$scope.plate = plate;
		});

		$scope.editPlate = function(plate) {
    		plateModal(CRUD_METHODS.edit, plate)
    		.then( function(updatedPlate){
                if (updatedPlate) {
                	console.log("plate",plate);
                	console.log("updated", updatedPlate)
                    $scope.plate = updatedPlate;
                    // $scope.$apply;
                }
    		});
    	}

    	$scope.deletePlate = function(plate) {
    		plateModal(CRUD_METHODS.delete, plate)
    		.then( function(deletedPlate){
    			$location.path('/stamphub/plates')
    		})
    	}
	})
	.controller('plateModalCtrl', function ($scope, $rootScope, Restangular, Validate, CRUD_METHODS, plate, method, objName) {
		$scope.title;
		$scope.edit = true;
		switch (method) {
			case CRUD_METHODS.add:
				$scope.title = 'Add Plate';
				break;
			case CRUD_METHODS.edit:
				$scope.title = 'Edit Plate';
				break;
			case CRUD_METHODS.delete:
				$scope.title = 'Confirm Delete';
				break;
			default:
				$scope.title = 'Plate Form';
		}

		// Now fill the form with initial values
        $scope.plate = plate;
        $scope.object = plate;
        $scope.objName = objName;
        var canceling = false;

		// Prepopulate the dropdown selections for brand and collection
		Restangular.all('platebrands').getList().then(function(brands) {
        	$scope.availableBrands = brands;
        	if (plate.brand > 0) {
				// If there is a brand selected on modal open, this is the edit modal
				// We need to populate the collections dropdown and select the provided
				// collection
				$scope.brandSelected();
	        } else {
	        	$scope.availableCollections = [];
	        }
        })        

        $scope.brandSelected = function() {
        	Restangular.one('platebrands', $scope.plate.brand)
        	.getList('collections')
        	.then(function(collections) {
        		$scope.availableCollections = collections;
        	})
        };
        function createObjectURL(object) {
		    return (window.URL) ? window.URL.createObjectURL(object) : window.webkitURL.createObjectURL(object);
		}
        $scope.fileSelected = function(files) {
        	console.log('file selected ' + files[0]);
        	console.log(files[0]);
        	$scope.plate.picture = createObjectURL(files[0])
        	console.log($scope.plate.picture);
        }

        $scope.savePlateForm = function(formData){
        	if (!canceling){
        		// We have to do this because in angular the form always
        		// submits itself when you close the modal, which is NO GOOD
        		console.log($scope.platePhoto)
	        	if (method == CRUD_METHODS.add) {
	        		$scope.addPlate(formData);
	        	} else if (method == CRUD_METHODS.edit){
	        		$scope.editPlate(formData);
	        	}
        	}
        }

        $scope.addPlate = function(formData){
            $scope.errors = [];
            Validate.form_validation(formData,$scope.errors);
            if(!formData.$invalid){
            	var file = $scope.platePhoto;
            	var fd = new FormData();
            	fd.append('picture', file);
            	// Append all the other properties
            	for(var property in $scope.plate) {
            		fd.append(property, $scope.plate[property])
            	}

            	Restangular.all('plates')
            	.withHttpConfig({
            		transformRequest: angular.identity
            	})
            	.customPOST(fd, '', undefined, {'Content-Type': undefined})
            	.then(function(data){
            		$scope.$close(data);
            	})
            	.catch(function(data){
            		$scope.errors = data.data;
            	});
            }
        }

        $scope.editPlate = function(formData){
        	$scope.errors = [];
            Validate.form_validation(formData,$scope.errors);
            if(!formData.$invalid){
                // $scope.plate is a Restangular object so we can call put on it
                var file = $scope.platePhoto;
            	var fd = new FormData();
            	// Append the picture
            	fd.append('picture', file);
            	// Append all the other properties
            	for(var property in $scope.plate) {
            		fd.append(property, $scope.plate[property])
            	}

        		$scope.plate
        		.withHttpConfig({
        			transformRequest: angular.identity
        		})
        		.customPUT(fd, undefined, undefined, {'Content-Type': undefined})
                .then(function(data){
                    $scope.$close(data);
                })
                .catch(function(data){
                    $scope.errors = data.data;
                });
            }
        }

        $scope.deleteObject = function(plate) {
            // If we got here we clicked Yes on the delete confirmation, so go 
            // ahead and delete
            plate.remove()
            .then(function(data){
            	$scope.$close(plate);
            })
            .catch(function(data){
            	console.log(data);
            	$scope.errors = ["500 Internal Server Error. Try Again Later.", ]
           })
        };

        $scope.cancel = function(){
        	canceling = true;
            $scope.$dismiss();
        }
    })
	// Service to open the plate cru modal
    .service('plateModal', function($modal, $rootScope, CRUD_METHODS) {
        return function(method, plateEdit) {
            var templateUrl, objName, plate;
            if (plateEdit) {
                plate = plateEdit;
                objName = plateEdit.number;
            } else {
                plate = {brand:0, collection:0, number:'',picture:undefined,picture_credit:''}
                objName = '';
            }
        	if (method == CRUD_METHODS.delete) {
                templateUrl = '/static/stamphub/confirm_delete.html';
            } else {
                templateUrl = '/static/stamphub/plates/plate_cru_modal.html';
            }

            var modalInstance = $modal.open({
                templateUrl: templateUrl,
                controller: 'plateModalCtrl',
                controllerAs: 'plateModalCtrl',
                resolve: {
                	plate: function() {
                		return plate;
                	},
                	method: function() {
                		return method;
                	},
                    objName: function() {
                        return objName;
                    }
                }
            });

            return modalInstance.result;
        };
    });