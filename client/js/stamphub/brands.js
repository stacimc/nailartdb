angular.module('nailArtApp')
	// PlateBrand controllers
	.controller('plateBrandListController', function($scope, $state, Restangular, $filter, brandModal, CRUD_METHODS) {
		// GET /api/platecollections
		Restangular.all('platebrands').getList().then(function(brands) {
			$scope.brands = brands;

			// Populate filteredBrands with all brands at first
			$scope.filteredBrands = brands;
			$scope.figureOutBrandsToDisplay();
		});

		$scope.currentPage = 1;
		$scope.brandsPerPage = 4;
		$scope.displayedBrands = [];

		// Returns the list of items to display on the current page, after filtering
		$scope.figureOutBrandsToDisplay = function() {
    		var begin = (($scope.currentPage - 1) * $scope.brandsPerPage);
    		var end = begin + $scope.brandsPerPage;

    		$scope.displayedBrands = $scope.filteredBrands.slice(begin, end);
    	};

    	$scope.pageChanged = function() {
    		$scope.figureOutBrandsToDisplay();
    	};

    	$scope.search = function() {
    		// Brands just has a basic search on brand name
    		$scope.filteredBrands = $filter('filter')($scope.brands, $scope.searchtext);
    		
    		// Reset to page 1 of search results
    		$scope.currentPage = 1;
    		$scope.figureOutBrandsToDisplay();
    	};

    	// CRUD actions

    	$scope.addBrand = function() {
    		brandModal(CRUD_METHODS.add)
    		.then( function(newBrand){
    			// Push the new brand to the list so that it will be displayed without
    			// having to refresh the page first
                if(newBrand) {
                    $scope.filteredBrands.push(newBrand);
                    $scope.figureOutBrandsToDisplay();
                }
    		});
    	}

    	$scope.editBrand = function(brand) {
    		brandModal(CRUD_METHODS.edit, brand)
    		.then( function(updatedBrand){
                if (updatedBrand) {
                    brand = updatedBrand;
                }
    		});
    	}

    	$scope.deleteBrand = function(brand) {
    		brandModal(CRUD_METHODS.delete, brand)
    		.then( function(deletedBrand){
                var index = $scope.filteredBrands.indexOf(deletedBrand);
                if (index > -1) {
                    $scope.filteredBrands.splice(index, 1);
                    $scope.figureOutBrandsToDisplay();
                }
    			console.log("We're back");
    		})
    	}
	})
	.controller('plateBrandDetailController', function($scope, $state, $stateParams, Restangular) {
		// GET /api/platebrands/id
		Restangular.one('platebrands', $stateParams.id).get().then(function(brand) {
			$scope.brand = brand;
		});
	})
	.controller('brandModalCtrl', function ($scope, $rootScope, Restangular, Validate, CRUD_METHODS, brand, method, objName) {
		$scope.title;
		switch (method) {
			case CRUD_METHODS.add:
				$scope.title = 'Add Brand';
				break;
			case CRUD_METHODS.edit:
				$scope.title = 'Edit Brand';
				break;
			case CRUD_METHODS.delete:
				$scope.title = 'Confirm Delete';
				break;
			default:
				$scope.title = 'Brand Form';
		}

        $scope.brand = brand;
        $scope.object = brand;
        $scope.objName = objName;
        var canceling = false;

        $scope.saveBrandForm = function(formData){
            if (!canceling){
                // We have to do this because in angular the form always
                // submits itself when you close the modal, which is NO GOOD
            	if (method == CRUD_METHODS.add) {
            		$scope.addBrand(formData);
            	} else if (method == CRUD_METHODS.edit){
            		$scope.editBrand(formData);
            	}
            }
        }

        $scope.addBrand = function(formData){
            $scope.errors = [];
            Validate.form_validation(formData,$scope.errors);
            if(!formData.$invalid){
                Restangular.all('platebrands').post($scope.brand)
                .then(function(data){
                    $scope.$close($scope.brand);
                })
                .catch(function(data){
                    $scope.errors = data.data;
                });
            }
        }

        $scope.editBrand = function(formData){
        	$scope.errors = [];
            Validate.form_validation(formData,$scope.errors);
            if(!formData.$invalid){
                // $scope.brand is a Restangular object so we can call put on it
        		$scope.brand.put()
                .then(function(data){
                    $scope.$close(data);
                })
                .catch(function(data){
                    $scope.errors = data.data;
                });
            }
        }

        $scope.deleteObject = function(brand) {
            // If we got here we clicked Yes on the delete confirmation, so go 
            // ahead and delete
            brand.remove()
            .then(function(data){
                $scope.$close(brand);
            })
            .catch(function(data){
                console.log(data);
                $scope.errors = ["500 Internal Server Error. Try Again Later.", ]
           })
        };

        $scope.cancel = function(){
            canceling = true;
            $scope.$dismiss();
        }
    })
	// Service to open the brand cru modal
    .service('brandModal', function($modal, $rootScope, CRUD_METHODS) {
        return function(method, brandEdit) {
            var templateUrl, objName, brand;
            if (brandEdit) {
                brand = brandEdit;
                objName = brandEdit.name;
            } else {
                brand = {name:'',preferred:false, website:'', creator:''}
                objName = '';
            }
        	if (method == CRUD_METHODS.delete) {
                templateUrl = '/static/stamphub/confirm_delete.html';
            } else {
                templateUrl = '/static/stamphub/brands/brand_cru_modal.html';
            }

            var modalInstance = $modal.open({
                templateUrl: templateUrl,
                controller: 'brandModalCtrl',
                controllerAs: 'brandModalCtrl',
                resolve: {
                	brand: function() {
                		return brand;
                	},
                	method: function() {
                		return method;
                	},
                    objName: function() {
                        return objName;
                    }
                }
            });

            return modalInstance.result;
        };
    });