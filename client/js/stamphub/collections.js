angular.module('nailArtApp')
	// PlateCollection controllers
	.controller('plateCollectionListController', function($scope, $state, Restangular, $filter, $stateParams, collectionModal, CRUD_METHODS) {

		// GET /api/platecollections
		Restangular.all('platecollections').getList().then(function(collections) {
			// Use stateParams to determine if we are in the main collection list or viewing
			// the collection list for a particular brand
			if ($stateParams.id){
				// We are in a subcontent panel, just get the collections for this brand
				$scope.collections = $filter('filter')(collections, {brand: $stateParams.id});
			} else {
				// We are in the main Collection list page. Get all collections
				$scope.collections = collections;
			}

			// Populate filteredCollections with all collections at first
			$scope.filteredCollections = $scope.collections;
			$scope.figureOutCollectionsToDisplay();
		});

		$scope.currentPage = 1;
		$scope.collectionsPerPage = 4;
		$scope.displayedCollections = [];

		$scope.figureOutCollectionsToDisplay = function() {
    		var begin = (($scope.currentPage - 1) * $scope.collectionsPerPage);
    		var end = begin + $scope.collectionsPerPage;

    		$scope.displayedCollections = $scope.filteredCollections.slice(begin, end);
    		// console.log($scope.displayedCollections)
    	};

    	$scope.pageChanged = function() {
    		$scope.figureOutCollectionsToDisplay();
    	};

    	$scope.search = function() {
    		// Collections has a search on its 'full_name' property, which is the brand name and 
    		// collection name
    		var results = $scope.collections
    		// We want search terms filtered independently, so that a search of "Pueen 2013" will
    		// return Pueen collections with 2013 in the name even if they don't appear in that
    		// order in the full_name
    		var searchTerms = ($scope.searchtext).split(' ');
    		for (var i=0; i < searchTerms.length; i++) {
    			results = $filter('filter')(results, {full_name: searchTerms[i]});
    		};
    		$scope.filteredCollections = results;
    		// Reset to page 1 of search results
    		$scope.currentPage = 1;
    		$scope.figureOutCollectionsToDisplay();
    	};

    	// CRUD actions

    	$scope.addCollection = function() {
    		collectionModal(CRUD_METHODS.add)
    		.then( function(newCollection){
    			// Push the new collection to the list so that it will be displayed without
    			// having to refresh the page first
                if(newCollection) {
                    $scope.filteredCollections.push(newCollection);
                    $scope.figureOutCollectionsToDisplay();
                }
    		});
    	}

    	$scope.editCollection = function(collection) {
    		collectionModal(CRUD_METHODS.edit, collection)
    		.then( function(updatedCollection){
                if (updatedCollection) {
                	console.log("collec",collection);
                	console.log("updated", updatedCollection)
                    collection = updatedCollection;
                }
    		});
    	}

    	$scope.deleteCollection = function(collection) {
    		collectionModal(CRUD_METHODS.delete, collection)
    		.then( function(deletedCollection){
                var index = $scope.filteredCollections.indexOf(deletedCollection);
                if (index > -1) {
                    $scope.filteredCollections.splice(index, 1);
                    $scope.figureOutCollectionsToDisplay();
                }
    			console.log("We're back");
    		})
    	}
	})
	.controller('plateCollectionDetailController', function($scope, $state, $stateParams, Restangular) {
		// GET /api/platecollections/id
		Restangular.one('platecollections', $stateParams.id).get().then(function(collection) {
			$scope.collection = collection;
		});
	})
	.controller('collectionModalCtrl', function ($scope, $rootScope, Restangular, Validate, CRUD_METHODS, collection, method, objName) {
		$scope.title;
		switch (method) {
			case CRUD_METHODS.add:
				$scope.title = 'Add Collection';
				break;
			case CRUD_METHODS.edit:
				$scope.title = 'Edit Collection';
				break;
			case CRUD_METHODS.delete:
				$scope.title = 'Confirm Delete';
				break;
			default:
				$scope.title = 'Collection Form';
		}

        $scope.collection = collection;
        $scope.object = collection;
        $scope.objName = objName;
        var canceling = false;

		Restangular.all('platebrands').getList().then(function(brands) {
        	$scope.availableBrands = brands;
        })

        $scope.saveCollectionForm = function(formData){
        	if (!canceling){
        		// We have to do this because in angular the form always
        		// submits itself when you close the modal, which is NO GOOD
	        	if (method == CRUD_METHODS.add) {
	        		$scope.addCollection(formData);
	        	} else if (method == CRUD_METHODS.edit){
	        		$scope.editCollection(formData);
	        	}
        	}
        }

        $scope.addCollection = function(formData){
            $scope.errors = [];
            Validate.form_validation(formData,$scope.errors);
            if(!formData.$invalid){
                Restangular.all('platecollections').post($scope.collection)
                .then(function(data){
                    $scope.$close($scope.collection);
                })
                .catch(function(data){
                    $scope.errors = data.data;
                });
            }
        }

        $scope.editCollection = function(formData){
        	$scope.errors = [];
            Validate.form_validation(formData,$scope.errors);
            if(!formData.$invalid){
                // $scope.collection is a Restangular object so we can call put on it
        		$scope.collection.put()
                .then(function(data){
                    $scope.$close($scope.collection);
                })
                .catch(function(data){
                    $scope.errors = data.data;
                });
            }
        }

        $scope.deleteObject = function(collection) {
            // If we got here we clicked Yes on the delete confirmation, so go 
            // ahead and delete
            collection.remove()
            .then(function(data){
            	$scope.$close(collection);
            })
            .catch(function(data){
            	console.log(data);
            	$scope.errors = ["500 Internal Server Error. Try Again Later.", ]
           })
        };

        $scope.cancel = function(){
        	canceling = true;
            $scope.$dismiss();
        }
    })
	// Service to open the collection cru modal
    .service('collectionModal', function($modal, $rootScope, CRUD_METHODS) {
        return function(method, collectionEdit) {
            var templateUrl, objName, collection;
            if (collectionEdit) {
                collection = collectionEdit;
                objName = collectionEdit.name;
            } else {
                collection = {brand:0, name:'',notes:'', review_url:''}
                objName = '';
            }
        	if (method == CRUD_METHODS.delete) {
                templateUrl = '/static/stamphub/confirm_delete.html';
            } else {
                templateUrl = '/static/stamphub/collections/collection_cru_modal.html';
            }

            var modalInstance = $modal.open({
                templateUrl: templateUrl,
                controller: 'collectionModalCtrl',
                controllerAs: 'collectionModalCtrl',
                resolve: {
                	collection: function() {
                		return collection;
                	},
                	method: function() {
                		return method;
                	},
                    objName: function() {
                        return objName;
                    }
                }
            });

            return modalInstance.result;
        };
    });