angular.module('nailArtApp')
    // Main controller which others will inherit from
    // Provides methods to determine authentication status from within the templates
    .controller('AuthCtrl', function($scope, $rootScope, USER_ROLES, AuthService){
        $scope.isAuthorized = AuthService.isAuthorized;
        $scope.isAuthenticated = AuthService.isAuthenticated;
        $scope.userRoles = USER_ROLES;
    })
    // Controller for the navbar
    // Controls all the basic user auth functions
    .controller('NavCtrl', function($scope, $location, AuthService, loginModal, registerModal){
        $scope.login = function(){
            loginModal();
        };

        $scope.logout = function(){
            AuthService.logout();
            // Go home
            $location.path('home');
        };

        $scope.register = function(){
            registerModal();
        }
    })
    // Service to open the login modal
    .service('loginModal', function($modal, $rootScope) {
        return function() {
            var modalInstance = $modal.open({
                templateUrl: '/static/subscribers/loginmodal.html',
                controller: 'LoginModalCtrl',
                controllerAs: 'LoginModalCtrl'
            });

            return modalInstance.result;
        };
    })
    // Service to open the registration modal
    .service('registerModal', function($modal, $rootScope){
        return function(){
            var modalInstance = $modal.open({
                templateUrl: '/static/subscribers/registermodal.html',
                controller: 'RegisterModalCtrl'
            });
            return modalInstance.result;
        }
    })
    .controller('LoginModalCtrl', function ($scope, $rootScope, AuthService, Validate, AUTH_EVENTS) {
        $scope.user = {};

        $scope.model = {'username':'','password':''};
        $scope.complete = false;
        $scope.login = function(formData){
            // AuthService.clearCredentials();

            $scope.errors = [];
            Validate.form_validation(formData,$scope.errors);
            if(!formData.$invalid){
                AuthService.login($scope.model.username, $scope.model.password)
                .then(function(data){
                    // Login successful: set the cookie
                    AuthService.setCredentials($scope.model.username, $scope.model.password);
                    $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);

                    $scope.$close(data);
                })
                .catch(function(data){
                    // Login failed
                    $rootScope.$broadcast(AUTH_EVENTS.loginFailed);
                    $scope.errors = data;
                });
            }
        }
        $scope.cancel = $scope.$dismiss
    })
    .controller('RegisterModalCtrl', function($scope, $rootScope, AuthService, Validate, AUTH_EVENTS){
        $scope.user = {};

        $scope.model = {'username':'','password':'','email':''};
        $scope.complete = false;

        $scope.register = function(formData){
          $scope.errors = [];
          Validate.form_validation(formData,$scope.errors);
          if(!formData.$invalid){
            AuthService.register($scope.model.username,$scope.model.password1,$scope.model.password2,$scope.model.email)
            .then(function(data){
                // Registration was a success
                $scope.complete = true;
                // Go ahead and log the user in
                AuthService.login($scope.model.username, $scope.model.password1)
                .then(function(data){
                    // Login was successful: set the cookie
                    AuthService.setCredentials($scope.model.username, $scope.model.password);
                    $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);

                    $scope.$close(data);
                })
                .catch(function(data){
                    $rootScope.$broadcast(AUTH_EVENTS.loginFailed);
                    $scope.errors = data;
                    $scope.nonFieldErrors = data['non_field_errors'];
                });
            },function(data){
                // error case
                $scope.errors = data;
                $scope.nonFieldErrors = data['non_field_errors'];
            });
          };
        };
    });

    