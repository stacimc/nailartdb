angular.module('nailArtApp')
    .run(function($rootScope, $state, loginModal, AuthService, AUTH_EVENTS, $cookieStore, $http) {

        // Restore the current user from the cookie on page refresh if possible
        $rootScope.globals = $cookieStore.get('globals') || {};
        if($rootScope.globals.currentUser){
            $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata;
        }

        // Authorization check on page transition
        $rootScope.$on('$stateChangeStart', function(event, toState, toParams) {
            // Check if the toState has a list of authorizedRoles; if not, pass through
            if (toState.data){
                var authorizedRoles = toState.data.authorizedRoles;

                // Check if the current user's role is on the list of authorized roles
                if(!AuthService.isAuthorized(authorizedRoles)){
                    event.preventDefault();
                    if (AuthService.isAuthenticated()){
                        // user is logged in, but does not have authorization
                        $rootScope.$broadcast(AUTH_EVENTS.notAuthorized);
                    } else {
                        // user is not logged in
                        $rootScope.$broadcast(AUTH_EVENTS.notAuthenticated);
                        // Open the login modal
                        loginModal()
                            .then(function(){
                                // On successful login, go ahead to the next page
                                return $state.go(toState.name, toParams);
                            })
                            .catch(function(){
                                // Else go back to the home page
                                return $state.go('home');
                            });
                    }
                };
            }
        })
    })
    // Intercept auth events
    // https://medium.com/opinionated-angularjs/techniques-for-authentication-in-angularjs-applications-7bbf0346acec#.b1wuhofmc
    .config(function($httpProvider){
        $httpProvider.interceptors.push([
            '$injector',
            function($injector){
                return $injector.get('AuthInterceptor');
            }
        ]);
    })
    .factory('AuthInterceptor', function($rootScope, $q, AUTH_EVENTS){
        return {
            responseError: function (response) { 
              $rootScope.$broadcast({
                401: AUTH_EVENTS.notAuthenticated,
                403: AUTH_EVENTS.notAuthorized,
                419: AUTH_EVENTS.sessionTimeout,
                440: AUTH_EVENTS.sessionTimeout
              }[response.status], response);
              return $q.reject(response);
            }
        };
    })        
    .constant('AUTH_EVENTS', {
        loginSuccess: 'user-login-success',
        loginFailed: 'user-login-failed',
        logoutSuccess: 'user-logout-success',
        sessionTimeout: 'user-session-timeout',
        notAuthenticated: 'user-not-authenticated',
        notAuthorized: 'user-not-authorized'
    })
    .constant('USER_ROLES', {
        all: '*',
        admin: 'admin',
        editor: 'editor',
        guest: 'guest'
    });