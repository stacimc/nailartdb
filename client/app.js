var nailArtApp = angular.module('nailArtApp', ['ui.router', 'restangular', 'ui.bootstrap', 'ngResource', 'ngCookies', 'ngSanitize', 'ngRoute',])
    .config(function(RestangularProvider, $httpProvider) {

        var newBaseUrl = 'http:localhost:8000/api'
        RestangularProvider.setBaseUrl('/api');

        // Django and angular both support csrf tokens. This tells
        // angular which cookie to add to which header
        $httpProvider.defaults.xsrfCookieName = 'csrftoken';
        $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
    });

nailArtApp.config(function($stateProvider, $urlRouterProvider) {
    
    $urlRouterProvider.otherwise('/home');
    
    $stateProvider
        
        .state('base', {
            abstract: true,
            url: '',
            templateUrl: '/static/main.html',
            controller: 'AuthCtrl',
        })
        .state('home', {
            parent: 'base',
            url: '/home',
            templateUrl: '/static/home.html',
        })
        
        .state('stamphub', {
            abstract:true,
            parent: 'base', // Children of stamphub will also inherit from base
            url: '/stamphub',
            templateUrl: '/static/stamphub/stamphub.html',
            data: {
                // This property will apply to all children of 'stamphub'
                authorizedRoles: ['admin',]
            }
        })
        .state('stamphub.image_list', {
            url: '/images',
            views: {
                'content': {
                    templateUrl: '/static/stamphub/images/image_list.html',
                    controller: 'imageListController'
                },
                'sidebar': {
                    templateUrl: '/static/stamphub/images/image_sidebar.html'
                }
            }
        })
        .state('stamphub.plate_list', {
            url: '/plates',
            views: {
                'content': {
                    templateUrl: '/static/stamphub/plates/plate_list.html',
                    controller:'plateListController'
                },
                'sidebar': {
                    templateUrl: '/static/stamphub/plates/plate_sidebar.html'
                }
            }
        })
        .state('stamphub.plate_detail', {
            url: '/plates/:id',
            views: {
                'content': {
                    templateUrl: '/static/stamphub/plates/plate_detail.html',
                    controller:'plateDetailController'
                },
                'subcontent': {
                    templateUrl: '/static/stamphub/images/image_list.html',
                    controller: 'imageListController'
                },
                'sidebar': {
                    templateUrl: '/static/stamphub/plates/plate_sidebar.html'
                }
            }
        })
        .state('stamphub.collection_list', {
            url: '/collections',
            views: {
                'content': {
                    templateUrl: '/static/stamphub/collections/collection_list.html',
                    controller: 'plateCollectionListController'
                },
                'sidebar': {
                    templateUrl: '/static/stamphub/collections/collection_sidebar.html'
                }
            }
        })
        .state('stamphub.collection_detail', {
            url: '/collections/:id',
            views: {
                'content': {
                    templateUrl: '/static/stamphub/collections/collection_detail.html',
                    controller: 'plateCollectionDetailController'
                },
                'subcontent': {
                    templateUrl: '/static/stamphub/plates/plate_list.html',
                    controller: 'plateListController'
                },
                'sidebar': {
                    templateUrl: '/static/stamphub/collections/collection_sidebar.html'
                }
            }
        })
        .state('stamphub.brand_list', {
            url: '/brands',
            views: {
                'content': {
                    templateUrl: '/static/stamphub/brands/brand_list.html',
                    controller: 'plateBrandListController'
                },
                'sidebar': {
                    templateUrl: '/static/stamphub/brands/brand_sidebar.html'
                }
            }
        })
        .state('stamphub.brand_detail', {
            url: '/brands/:id',
            views: {
                'content': {
                    templateUrl: '/static/stamphub/brands/brand_detail.html',
                    controller: 'plateBrandDetailController'
                },
                'subcontent': {
                    templateUrl: '/static/stamphub/collections/collection_list.html',
                    controller: 'plateCollectionListController'
                },
                'sidebar': {
                    templateUrl: '/static/stamphub/brands/brand_sidebar.html'
                }
            }
        })
        
});