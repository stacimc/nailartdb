# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('stamphub', '0002_auto_20160902_1746'),
    ]

    operations = [
        migrations.AlterField(
            model_name='platebrand',
            name='name',
            field=models.CharField(max_length=40, unique=True),
        ),
        migrations.AlterField(
            model_name='platecollection',
            name='name',
            field=models.CharField(max_length=100, unique=True),
        ),
    ]
