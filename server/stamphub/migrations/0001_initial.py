# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import stamphub.models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Image',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('img_type', models.CharField(max_length=10, blank=True, choices=[('BUFFET', 'Buffet'), ('FULL', 'Full image'), ('HALFMOON', 'Full image with half moon'), ('INDIV', 'Individual element'), ('HALF', 'Half image, not diagonal'), ('DIAGONAL', 'Half image, diagonal'), ('FRENCH', 'French'), ('VSTRIPE', 'Vertical Stripe'), ('HSTRIPE', 'Horizontal Stripe'), ('DSTRIPE', 'Diagonal Stripe')])),
                ('size', models.CharField(max_length=3, blank=True, choices=[('L', 'Too big'), ('M', 'Perfect'), ('S', 'Too small')])),
                ('picture', models.ImageField(storage=stamphub.models.OverwriteStorage(), blank=True, upload_to=stamphub.models.get_image_photo_path)),
            ],
            options={
                'verbose_name_plural': 'images',
            },
        ),
        migrations.CreateModel(
            name='ImageRating',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('quality', models.CharField(max_length=10, blank=True, choices=[('GREAT', 'Great'), ('GOOD', 'Good'), ('BAD', 'Bad'), ('AWFUL', 'Awful')])),
                ('image', models.ForeignKey(to='stamphub.Image')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='ImageTagging',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('image', models.ForeignKey(to='stamphub.Image')),
            ],
        ),
        migrations.CreateModel(
            name='Plate',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('number', models.CharField(max_length=25, unique=True)),
                ('picture', models.ImageField(storage=stamphub.models.OverwriteStorage(), blank=True, upload_to=stamphub.models.get_plate_photo_path)),
                ('picture_credit', models.CharField(max_length=45, null=True)),
            ],
            options={
                'verbose_name_plural': 'plates',
            },
        ),
        migrations.CreateModel(
            name='PlateBrand',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('name', models.CharField(max_length=40)),
                ('preferred', models.BooleanField(default=True)),
                ('website', models.URLField(blank=True, null=True)),
                ('creator', models.CharField(blank=True, max_length=50, null=True)),
            ],
            options={
                'verbose_name_plural': 'brands',
            },
        ),
        migrations.CreateModel(
            name='PlateCollection',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('name', models.CharField(max_length=100)),
                ('notes', models.TextField(blank=True, null=True)),
                ('review_url', models.URLField(blank=True, null=True)),
                ('plate_shape', models.CharField(max_length=2, choices=[('C', 'circle'), ('S', 'square'), ('LR', 'long rectangle'), ('WR', 'wide rectangle'), ('H', 'hexagon'), ('O', 'octagon'), ('F', 'full page')])),
                ('brand', models.ForeignKey(to='stamphub.PlateBrand')),
            ],
            options={
                'verbose_name_plural': 'collections',
            },
        ),
        migrations.CreateModel(
            name='Subscriber',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('prefer_limit_stampdb', models.BooleanField(default=False)),
                ('prefer_limit_polishdb', models.BooleanField(default=False)),
                ('prefer_limit_manidb', models.BooleanField(default=False)),
                ('public', models.BooleanField(default=False)),
                ('profile_pic', models.ImageField(storage=stamphub.models.OverwriteStorage(), blank=True, upload_to=stamphub.models.get_profile_photo_path)),
                ('user_rec', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name_plural': 'subscribers',
            },
        ),
        migrations.CreateModel(
            name='Tag',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('name', models.CharField(max_length=20, unique=True)),
                ('n_applied', models.IntegerField(default=0)),
            ],
            options={
                'verbose_name_plural': 'tags',
            },
        ),
        migrations.AddField(
            model_name='plate',
            name='collection',
            field=models.ForeignKey(null=True, to='stamphub.PlateCollection'),
        ),
        migrations.AddField(
            model_name='plate',
            name='owners',
            field=models.ManyToManyField(to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='imagetagging',
            name='tag',
            field=models.ForeignKey(to='stamphub.Tag'),
        ),
        migrations.AddField(
            model_name='imagetagging',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='image',
            name='plate',
            field=models.ForeignKey(to='stamphub.Plate'),
        ),
        migrations.AddField(
            model_name='image',
            name='tags',
            field=models.ManyToManyField(through='stamphub.ImageTagging', to='stamphub.Tag'),
        ),
    ]
