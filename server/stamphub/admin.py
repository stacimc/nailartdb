from django.contrib import admin
from .models import PlateBrand
from .models import PlateCollection
from .models import Plate
from .models import Image
from .models import Tag
from .models import ImageTagging
from .models import ImageRating

admin.site.register(PlateBrand)
admin.site.register(PlateCollection)
admin.site.register(Plate)
admin.site.register(Image)
admin.site.register(Tag)
admin.site.register(ImageTagging)
admin.site.register(ImageRating)