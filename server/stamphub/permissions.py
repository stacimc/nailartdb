from rest_framework import permissions


class IsAdminOrReadOnly(permissions.BasePermission):
	"""
	Allows everyone to list or view, but only admins can modify existing instances
	"""
	def has_object_permission(self, request, view, obj=None):
		if obj is None:
			# Either a list or a create, so safe for everyone
			can_edit = True
		else:
			can_edit = request.user and request.user.is_staff
		return can_edit or super(IsAdminOrReadOnly, self).has_object_permission(request, view, obj)