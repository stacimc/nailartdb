from django.shortcuts import render

from rest_framework import generics, permissions
from rest_framework.parsers import FormParser, MultiPartParser

from .permissions import IsAdminOrReadOnly
from .serializers import TagSerializer, ImageSerializer, PlateSerializer, PlateCollectionSerializer, PlateBrandSerializer
from .models import PlateBrand, PlateCollection, Plate, Image, Tag, ImageTagging, ImageRating


# Use django to render the index
def index(request):
	return render(request, 'index.html')

# Image API
class ImageList(generics.ListCreateAPIView):
	model = Image
	queryset = Image.objects.all()
	serializer_class = ImageSerializer
	permission_classes = [
		IsAdminOrReadOnly
	]

class ImageDetail(generics.RetrieveUpdateDestroyAPIView):
	model = Image
	queryset = Image.objects.all()
	serializer_class = ImageSerializer
	permission_classes = [
		IsAdminOrReadOnly
	]

# Plate API
class PlateList(generics.ListCreateAPIView):
	model = Plate
	queryset = Plate.objects.all()
	serializer_class = PlateSerializer
	permission_classes = [
		IsAdminOrReadOnly
	]
	parser_classes = (MultiPartParser, FormParser,)

	def perform_create(self, serializer):
		collection_id = self.request.data.get('collection')
		collection = PlateCollection.objects.filter(pk=collection_id).first()

		number = self.request.data.get('number')
		picture_credit = self.request.data.get('picture_credit')
		picture = self.request.data.get('picture')
		serializer.save(collection=collection, number=number, picture_credit=picture_credit, picture=picture)


class PlateDetail(generics.RetrieveUpdateDestroyAPIView):
	model = Plate
	queryset = Plate.objects.all()
	serializer_class = PlateSerializer
	permission_classes = [
		IsAdminOrReadOnly
	]
	parser_classes = (MultiPartParser, FormParser,)

	def perform_update(self, serializer):
		collection_id = self.request.data.get('collection')
		collection = PlateCollection.objects.filter(pk=collection_id).first()

		number = self.request.data.get('number')
		picture_credit = self.request.data.get('picture_credit')
		picture = self.request.data.get('picture')
		serializer.save(collection=collection, number=number, picture_credit=picture_credit, picture=picture)


class PlateImageList(generics.ListAPIView):
	model = Image
	queryset = Image.objects.all()
	serializer_class = ImageSerializer
	permission_classes = [
		IsAdminOrReadOnly
	]

	def get_queryset(self):
		queryset = super(PlateImageList, self).get_queryset()
		return queryset.filter(plate__pk=self.kwargs.get('pk'))

# PlateCollection API
class PlateCollectionList(generics.ListCreateAPIView):
	model = PlateCollection
	queryset = PlateCollection.objects.all()
	serializer_class = PlateCollectionSerializer
	permission_classes = [
		IsAdminOrReadOnly
	]

class PlateCollectionDetail(generics.RetrieveUpdateDestroyAPIView):
	model = PlateCollection
	queryset = PlateCollection.objects.all()
	serializer_class = PlateCollectionSerializer
	permission_classes = [
		IsAdminOrReadOnly
	]

class PlateCollectionPlateList(generics.ListAPIView):
	model = Plate
	queryset = Plate.objects.all()
	serializer_class = PlateSerializer
	permission_classes = [
		IsAdminOrReadOnly
	]

	def get_queryset(self):
		queryset = super(PlateCollectionPlateList, self).get_queryset()
		return queryset.filter(collection__pk=self.kwargs.get('pk'))

# PlateBrand API
class PlateBrandList(generics.ListCreateAPIView):
	model = PlateBrand
	queryset = PlateBrand.objects.all()
	serializer_class = PlateBrandSerializer
	permission_classes = [
		IsAdminOrReadOnly
	]

class PlateBrandDetail(generics.RetrieveUpdateDestroyAPIView):
	model = PlateBrand
	queryset = PlateBrand.objects.all()
	serializer_class = PlateBrandSerializer
	permission_classes = [
		IsAdminOrReadOnly
	]

class PlateBrandCollectionList(generics.ListAPIView):
	model = PlateCollection
	queryset = PlateCollection.objects.all()
	serializer_class = PlateCollectionSerializer
	permission_classes = [
		IsAdminOrReadOnly
	]

	def get_queryset(self):
		queryset = super(PlateBrandCollectionList, self).get_queryset()
		return queryset.filter(brand__pk=self.kwargs.get('pk'))