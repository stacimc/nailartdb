from django.conf.urls import include, url
from django.contrib import admin

from django.conf import settings
from django.conf.urls.static import static

from subscribers.views import AuthView, UserView

from stamphub.views import ImageList, ImageDetail
from stamphub.views import PlateList, PlateDetail, PlateImageList
from stamphub.views import PlateCollectionList, PlateCollectionDetail, PlateCollectionPlateList
from stamphub.views import PlateBrandList, PlateBrandDetail, PlateBrandCollectionList
from stamphub import views as stamphub_views

urlpatterns = [

    url(r'^admin/', include(admin.site.urls)),
    url(r'^index/$', stamphub_views.index, name='index'),

    # Needed for django-rest-auth
    url(r'^', include('django.contrib.auth.urls')),
    url(r'^rest-auth/', include('rest_auth.urls')),
    url(r'^rest-auth/registration/', include('rest_auth.registration.urls')),

    # API URLs
    
    url(r'^api/images$', ImageList.as_view(), name='image_list'),
    url(r'^api/images/(?P<pk>\d+)$', ImageDetail.as_view(), name='image_detail'),

    url(r'^api/plates$', PlateList.as_view(), name='plate_list'),
    url(r'^api/plates/(?P<pk>\d+)$', PlateDetail.as_view(), name='plate_detail'),
    url(r'^api/plates/(?P<pk>\d+)/images$', PlateImageList.as_view(), name='plate-image_list'),

    url(r'^api/platecollections$', PlateCollectionList.as_view(), name='plate-collection_list'),
    url(r'^api/platecollections/(?P<pk>\d+)$', PlateCollectionDetail.as_view(), name='plate-collection_detail'),
    url(r'^api/platecollections/(?P<pk>\d+)/plates$', PlateCollectionPlateList.as_view(), name='plate-collection-plate_list'),

    url(r'^api/platebrands$', PlateBrandList.as_view(), name='plate-brand_list'),
    url(r'^api/platebrands/(?P<pk>\d+)$', PlateBrandDetail.as_view(), name='plate-brand_detail'),
    url(r'^api/platebrands/(?P<pk>\d+)/collections$', PlateBrandCollectionList.as_view(), name='plate-brand-collection_list'),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
