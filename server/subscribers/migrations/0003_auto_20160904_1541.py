# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('subscribers', '0002_auto_20160904_1540'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userprofile',
            name='user_rec',
            field=models.OneToOneField(to=settings.AUTH_USER_MODEL),
        ),
    ]
