# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import subscribers.models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Subscriber',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('prefer_limit_stampdb', models.BooleanField(default=False)),
                ('prefer_limit_polishdb', models.BooleanField(default=False)),
                ('prefer_limit_manidb', models.BooleanField(default=False)),
                ('public', models.BooleanField(default=False)),
                ('profile_pic', models.ImageField(storage=subscribers.models.OverwriteStorage(), blank=True, upload_to=subscribers.models.get_profile_photo_path)),
                ('user_rec', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name_plural': 'subscribers',
            },
        ),
    ]
