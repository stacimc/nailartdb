# README #

NailArtDB is a web application for cataloging nail stamping plates.

### What is Stamping? ###

Nail Stamping is a popular technique in the nail art community. A metal plate is engraved with many small images. The nail artist paints over the image, scrapes off excess polish, and then uses a special nail stamper to transfer the image from the plate onto their nail.

### So What's the Problem? ###

Stamping fans may collect hundreds of stamping plates, each of which has many images. How to organize your collection? Imagine you want to look through the plates you own for images of roses. You find yourself flipping through binders of plates! And there's no easy way to see all such images side by side for easy comparison.

**NailArtDB** solves this problem by providing an easy way to tag and search all of your images. 

Register for an account in order to be given access to the Stamp-hub feature. Select what you want to search from the Stamp-hub menu: images, plates, collections, or brands. (A company of platemakers creates its own **brand**, who may release multiple **collections** of **plates**, each of which has several **images**.) All pages support filtering.

### Coming Soon ###

* Polish-hub: A similar tool for cataloging nail polish
* Mani-hub: A similar tool for cataloging manicures

### Screenshots ###

####Home Page####

![HomePage.PNG](https://bitbucket.org/repo/KoLRnB/images/207895990-HomePage.PNG)

####Image List####

![Images.PNG](https://bitbucket.org/repo/KoLRnB/images/3140305060-Images.PNG)

####Example of Image Search####

![ImageSearch.PNG](https://bitbucket.org/repo/KoLRnB/images/331348515-ImageSearch.PNG)

####Detail View of an Image####

![ImageDetail.PNG](https://bitbucket.org/repo/KoLRnB/images/1838752973-ImageDetail.PNG)

####Plate List####

![Plates.PNG](https://bitbucket.org/repo/KoLRnB/images/4070111451-Plates.PNG)

####Detail View of a Plate, showing a list of its images####

![PlateDetail.PNG](https://bitbucket.org/repo/KoLRnB/images/3567348432-PlateDetail.PNG)

####Collection List####

![Collections.PNG](https://bitbucket.org/repo/KoLRnB/images/2594400404-Collections.PNG)

####Detail View of a Collection, showing a list of its plates####

![CollectionDetail.PNG](https://bitbucket.org/repo/KoLRnB/images/2523339517-CollectionDetail.PNG)

####Brand List####

![Brands.PNG](https://bitbucket.org/repo/KoLRnB/images/2546548565-Brands.PNG)

####Detail View of a Brand, showing a list of its collections####

![BrandDetail.PNG](https://bitbucket.org/repo/KoLRnB/images/1100191475-BrandDetail.PNG)